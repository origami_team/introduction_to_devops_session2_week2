package com.libertymutual.student.spencer.programs.example01.shapes;

import org.junit.Test;

import java.awt.*;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class CircleTest {

    @Test
    public void testGetArea() {
        Circle circle = new Circle(10, Color.red);
        BigDecimal area = circle.getArea();
        // this might help https://www.calculatorsoup.com/calculators/geometry-plane/circle.php
        BigDecimal expectedAnswer = new BigDecimal(314);
        assertEquals("Verify that the area is correct", expectedAnswer, area);
    }

}
