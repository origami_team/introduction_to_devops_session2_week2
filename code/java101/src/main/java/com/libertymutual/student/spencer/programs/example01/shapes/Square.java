package com.libertymutual.student.spencer.programs.example01.shapes;

import java.awt.*;
import java.math.BigDecimal;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Square extends Shape {

    private final static Logger logger = LogManager.getLogger(Square.class);

    private int length;

    public Square(int length, Color color) {
        super(color);
        this.length = length;
        logger.info("Square constructed: " + toString());
    }

    // provide a getArea implementation
    @Override
    public BigDecimal getArea() {
        return new BigDecimal(length * 4);
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "Square{" +
                "length=" + length +
                "color=" + super.getColor() +
                '}';
    }
}