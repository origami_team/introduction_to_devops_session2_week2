package com.libertymutual.student.spencer.programs.example01.shapes;

import java.awt.*;
import java.math.BigDecimal;

abstract public class Shape {

    private Color color;

    public Shape(Color color) {
        this.color = color;
    }

    abstract BigDecimal getArea();

    Color getColor() {
        return color;
    }

}